= Asciidoc Example Page
:navtitle: Asciidoc Example
:source-highlighter: rouge
:rouge-css: style
:icons: font
:stem: latexmath
:imagesoutdir: ./images/

image::asciidoc-example-banner.png[]

== Having Conditions to Control Render

ifdef::my[]
The parameter *my* is set as {my} with text: `{my}`
endif::[]

ifndef::my[]
The parameter *my* isn't set.
endif::[]

== Diagrams
=== Alice and Bob

[plantuml,align=center,svg]
----
@startuml
Alice -> Bob: Authentication Request
Bob --> Alice: Authentication Response

Alice -> Bob: Another authentication Request
Alice <-- Bob: another authentication Response
@enduml
----

=== Wireframe
[plantuml,align=center]
----
@startsalt
{^"CSV File IMport"
--          |  --
Contains Header Line | { (X) Yes | () No }
Delimiter   | { () Tab | (X) Semicolon | () Comma |  () Colon}
Quote Sign  | { () None | (X) Single Quote | () Double Quote }
-- | --
{} { [Cancel] | [Save] }
}
@endsalt
----

=== State Diagram
[plantuml,align=center]
----
@startuml
[*] --> State1
State1 --> [*]
State1 : this is a string
State1 : this is another string

State1 -> State2
State2 --> [*]
@enduml
----

=== Testdot
[plantuml,align=center]
----
@startuml
testdot
@enduml
----

== Links

IMPORTANT: Add the line `uglyurls: true` to your HUGO configuration file.

[[first_link]]
=== First Link

An example and check how links works.

=== Define an Anchor

Asciidoctor defines automatic anchors for section names, but for stable links it's much better to use an own anchor defined .Title

[source,asciidoc]
----
[[some_anchor]]
----

In the document the short form to links is useful, like <<first_link>> for this section.

[source,asciidoc]
----
<<some_anchors>>
----

=== Default Link with Section
[source,asciidoc]
----
<<second_links.adoc#last,See Last Section>>
----

<<second_links.adoc#last,See Last Section>>

=== Xref Alternative
[source,asciidoc]
----
xref:second_links.adoc#last[2^nd^ document section last]
----

xref:second_links.adoc#last[2^nd^ document section last]

=== Hugo Link Syntax
----
link:{{< ref "second_links#last" >}}[Second Document Section Last]
----

link:{{< ref "second_links#last" >}}[Second Document Section Last]

NOTE: This is not recommended because we leave the asciidoc syntax.

== Inline Formatting

Asciidoc allows *bold* _italic_ and much^more^ styles.

== Admonitions

NOTE: **NOTE** this is a small note.

WARNING: **WARNING** this can be used to warn the reader!

CAUTION: **CAUTION** and this marks an error.

TIP: **TIP** do things better.

IMPORTANT: **IMPORTANT** this must be done, before start the engine.

== Tables

|===
| Number | Square | Even

| 12 | 144 | yes
| 4 | 16 | yes
| 3 | 9 | no
|===

== Some Formulas

----
stem:[\sum_{i=0}^n i^2 = \frac{(n^2+n)(2n+1)}{6}]
----

* stem:[\sum_{i=0}^n i^2 = \frac{(n^2+n)(2n+1)}{6}]

----
stem:[H_2O]
----
* Water (stem:[H_2O]) is a critical component.
----
stem:[C = \alpha + \beta Y^{\gamma} + \epsilon]
----
* stem:[C = \alpha + \beta Y^{\gamma} + \epsilon]

----
stem:[6\sigma]
----
Used as quality threshold is the stem:[6\sigma] based on normal didtribution and its standard deviation very popular. This means all results are in the range which covers nearly 100% of all values.

== Source Code Blocks

This is a small source snippet

[source,python]
----
if __name__ == "__main__": #<1>
    print("This is the main part.")
----
<1> check if the python file was started directly

[source,c]
.A C language example
----
#include <stdio>

void main(char *argc, char **argv)
{
    printf("This is a main method\n");
}
----
